-- Table des rôles
CREATE TABLE roles (
    id INTEGER NOT NULL PRIMARY KEY,
    name VARCHAR NOT NULL,
    metas JSON DEFAULT '{}'::JSON
);

-- Table des tags
CREATE TABLE tags (
    id INTEGER NOT NULL PRIMARY KEY,
    name VARCHAR NOT NULL
);

-- Enumération pour le genre
CREATE TYPE gender_enum AS ENUM ('male', 'female', 'non_binary');

-- Table de jonction pour la relation many-to-many entre `users` et `roles`
CREATE TABLE user_roles (
    user_id INTEGER REFERENCES users(id),
    role_id INTEGER REFERENCES roles(id),
    PRIMARY KEY (user_id, role_id)
);

-- Table des utilisateurs
CREATE TABLE users (
    id INTEGER NOT NULL PRIMARY KEY,
    firstname VARCHAR NOT NULL,
    lastname VARCHAR NOT NULL,
    pseudo VARCHAR,
    birthday TIMESTAMP WITH TIME ZONE,
    gender gender_enum DEFAULT 'non_binary' NOT NULL,
    phone VARCHAR,
    postal_code VARCHAR,
    address VARCHAR,
    city VARCHAR,
    country VARCHAR,
    registration_date TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
    end_date TIMESTAMP WITH TIME ZONE,
    close BOOLEAN DEFAULT false,
    metas JSON DEFAULT '{}'::JSON,
    personnal_mail VARCHAR,
    school_mail VARCHAR DEFAULT (
        LOWER(CONCAT(firstname, '_', lastname)) || '@isdan-school.com'
    ),
    login VARCHAR DEFAULT (
        LOWER(CONCAT(firstname, '_', lastname))
    ),
    password VARCHAR NOT NULL,
    user_personnal_token VARCHAR
);
