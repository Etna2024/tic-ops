from sqlalchemy import Column, Integer, String, JSON
from sqlalchemy.orm import relationship
from src.base import Base
from typing import Any
from pydantic import BaseModel, Json


class Role(Base):
    __tablename__ = "roles"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, nullable=False)
    metas = Column(JSON, default={})
    users = relationship('User', secondary='user_roles', back_populates='roles')


class RoleCreate(BaseModel):
    name: str
    metas: Json[Any] = None


class RoleUpdate(BaseModel):
    role_name: str
    new_name: str


class RoleDelete(BaseModel):
    role_name: str
