from sqlalchemy import Column, Integer, String
from src.base import Base
from pydantic import BaseModel


class Tag(Base):
    __tablename__ = "tags"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, nullable=False)


class TagCreate(BaseModel):
    name: str


class TagUpdate(BaseModel):
    tag_name: str
    new_name: str


class TagDelete(BaseModel):
    tag_name: str
