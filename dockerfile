FROM debian:11

# to update on the Debian operating system 
# and then install the `python3-pip` and `python3-venv` packages.
RUN apt-get update && apt-get install -y python3-pip python3-venv

WORKDIR /OPS2-API

ENV VIRTUAL_ENV=/OPS2-API/venv

RUN python3 -m venv $VIRTUAL_ENV

ENV PATH="$VIRTUAL_ENV/bin:$PATH"

COPY . .

RUN pip3 install wheel setuptools psycopg2-binary

# This command is used to install the Python packages specified in the `requirements.txt` file.
RUN  pip3 install -r requirements.txt

ENV DATABASE_HOST=localhost
ENV DATABASE_PORT=5432
ENV DATABASE_NAME=ops_db
ENV DATABASE_USER=ops_bot
ENV DATABASE_PASSWORD=toto
ENV SECRET_SALT=AZ676EOJ#huig5h#hu@83498zede
ENV SECRET_KEY=0178db2f2308621d29a2b3d32c9d092f9d50db7a9e7e3e7df76a42d5a776d837

EXPOSE 4242

CMD [ "python3", "-m", "gunicorn", "main:app", "-w", "2", "-b", "0.0.0.0:4242"]
