from fastapi.testclient import TestClient
from pathlib import Path
import sys
sys.path.append(str(Path(__file__).resolve().parent.parent))
from main import app

client = TestClient(app)


def test_get_users():
    response = client.get("/users/")
    assert response.status_code == 200


def test_user_by_login():
    response = client.get("/user/{user_login}")
    assert response.status_code == 200


def test_user_by_id():
    response = client.get("/user/{user_login}")
    assert response.status_code == 200
