from fastapi.testclient import TestClient
from pathlib import Path
import sys
sys.path.append(str(Path(__file__).resolve().parent.parent))
from main import app

client = TestClient(app)


def test_get_tags():
    response = client.get("/tags/")
    assert response.status_code == 200


def test_post_tag():
    tag_data = {
       'name': 'pedago'
    }
    response = client.post("/tag/add", json=tag_data)
    assert response.status_code == 200
    assert response.json() == {"message": "Tag added successfully"}


def test_post_tags():
    tag_data = ["achat", "revente"]
    response = client.post("/tags/add", json=tag_data)
    assert response.status_code == 200
    assert response.json() == {"message": "Tags added successfully"}


def test_update_tag_endpoint():
    tag_data = {
       "tag_name": "achat",
       "new_name": "intervention"
    }
    response = client.post("/tag/update", json=tag_data)
    assert response.status_code == 200
    assert response.json() == {"message": f"Tag '{tag_data['tag_name']}' updated successfully to '{tag_data['new_name']}'"}


def test_delete_tag_endpoint():
    tag_data = {
      "tag_name": "revente",
    }
    response = client.post("/tag/delete", json=tag_data)
    assert response.status_code == 200
    assert response.json() == {"message": f"Tag '{tag_data['tag_name']}' deleted successfully"}
