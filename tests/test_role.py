from fastapi.testclient import TestClient
from pathlib import Path
import sys
sys.path.append(str(Path(__file__).resolve().parent.parent))
from main import app

client = TestClient(app)


def test_get_roles():
    response = client.get("/roles/")
    assert response.status_code == 200


def test_post_role():
    role_data = {
        "name": "user",
    }
    response = client.post("/role/add", json=role_data)
    assert response.status_code == 200
    assert response.json() == {"message": "Role added successfully"}


def test_post_roles():
    role_data = ["etudiant", "prof"]
    response = client.post("/roles/add", json=role_data)
    assert response.status_code == 200
    assert response.json() == {"message": "Roles added successfully"}


def test_update_role_endpoint():
    role_data = {
          "role_name": "prof",
          "new_name": "intervenant"
    }
    response = client.post("/role/update", json=role_data)
    assert response.status_code == 200
    assert response.json() == {"message": f"Role '{role_data['role_name']}' "
                               f"updated successfully to '{role_data['new_name']}'"}


def test_delete_role_endpoint():
    role_data = {
      "role_name": "etudiant",
    }
    response = client.post("/role/delete", json=role_data)
    assert response.status_code == 200
    assert response.json() == {"message": f"Role '{role_data['role_name']}' deleted successfully"}
