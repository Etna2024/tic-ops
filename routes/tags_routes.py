from typing import List
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from models.tag_model import Tag, TagCreate, TagDelete, TagUpdate
from src.database import get_db
from src.tag_utils import add_tag, add_tags, delete_tag, update_tag

router = APIRouter()


@router.get("/tags/")
def get_tags(db: Session = Depends(get_db)):
    tags = db.query(Tag).all()
    return tags


@router.post("/tag/add")
def post_tag(tag_create: TagCreate, db: Session = Depends(get_db)):
    add_tag(tag_create.name, db)
    return {"message": "Tag added successfully"}


@router.post("/tags/add")
def post_tags(tag_names: List[str], db: Session = Depends(get_db)):
    add_tags(tag_names, db)
    return {"message": "Tags added successfully"}


@router.post("/tag/update")
def update_tag_endpoint(tag_update: TagUpdate, db: Session = Depends(get_db)):
    update_tag(tag_update.tag_name, tag_update.new_name, db)
    return {"message": f"Tag '{tag_update.tag_name}' updated successfully to '{tag_update.new_name}'"}


@router.post("/tag/delete")
def delete_tag_endpoint(tag_delete: TagDelete, db: Session = Depends(get_db)):
    delete_tag(tag_delete.tag_name, db)
    return {"message": f"Tag '{tag_delete.tag_name}' deleted successfully"}
